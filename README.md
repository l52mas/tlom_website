# hf_website

Esqueleto de proyecto para un modelo basado en la librería de huggingface


## Arquitectura del sistema

![Arquitectura del sistema](architecture.svg?raw=true "Arquitectura del sistema")


## Agradecimientos

Agradecemos el apouyo al proyecto “Traducci ´ on autom ´ atica ´
para lenguas ind´ıgenas de Mexico” PAPIIT-IA104420, UNAM.

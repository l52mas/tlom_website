include .env

.PHONY: help dc-start dc-stop dc-start-local dc-build

help: ## Show this help menu
	@echo "Usage: make [TARGET ...]"
	@echo ""
	@grep --no-filename -E '^[a-zA-Z_%-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
		awk 'BEGIN {FS = ":.*?## "}; {printf "%-15s %s\n", $$1, $$2}'

stop: ## Stop docker (might need sudo)
	@docker-compose stop;

start: dc-stop dc-build ## Start docker (might need sudo)
	@docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d;

local: dc-stop dc-build
	@docker-compose -f docker-compose.yml -f docker-compose.dev.yml up

build:
	@docker-compose -f docker-compose.yml -f docker-compose.dev.yml build
